<?php

	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';	
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code       
        $x = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
        if(isset($_REQUEST['movimiento']) &&  $_REQUEST['movimiento'] !== NULL){
            $movimiento = mysqli_real_escape_string($con,(strip_tags($_REQUEST['movimiento'], ENT_QUOTES)));
        }
        else{
            $movimiento = NULL;
        }
        //Inicio de transformar codigo KM
        $sql = "SELECT * FROM products WHERE codigo_producto = '".$x."'";
        $query = mysqli_query($con, $sql);
        $row=mysqli_fetch_array($query);
        $f = $row['id_producto'];
        
        if($f ===''){
            $q = 0;
        }
        else{
            $q = $f;
        }
        //Fin conversion codigo a KM
		 $aColumns = array('id_producto');//Columnas de busqueda
		 $sTable = "historial";
		 $sWhere = "";
		if ( $q != "" )
		{
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." = '".$q."' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
                else{
                    $sWhere = "WHERE id_producto = '0' ";
                }
                if ($movimiento !== ''){
			$sWhere .=" and movimiento='$movimiento'";
		}
                if($movimiento === ''){
                    $in = 'IN';
                    $out = 'OUT';
                    $sWhere .=" and movimiento IN('$in','$out') ";
                }
		$sWhere.=" order by fecha desc";
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 50; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable  $sWhere");                
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './historial.php';
		//main query to fetch the data
		$sql="SELECT * FROM  $sTable $sWhere LIMIT $offset,$per_page";               
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="success">
					<th>Fecha</th>
					<th>Nota</th>
					<th class='text-center'>Centro Costo</th>
					<th class='text-center'>Movimiento</th>
					<th>Referencia</th>
                                        <th class='text-center'>Solicitante</th>
                                        <th class='text-center'>Autorizo</th>
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						$fecha=date('d/m/Y', strtotime($row['fecha']));
						$nota=$row['nota'];
						$centrocosto=$row['centrocosto'];
						$movimiento= $row['movimiento'];
                                                $referencia= $row['referencia'];
						$solicitante= $row['solicitante'];
                                                $autorizo= $row['autorizo'];
					?>
					<tr>
						
						<td><?php echo $fecha; ?></td>
						<td ><?php echo $nota; ?></td>	
                                                <td class='text-center'><?php echo $centrocosto;?></td>
						<td class='text-center'><?php echo $movimiento;?></td>
                                                <td><?php echo $referencia;?></td>
                                                <td class='text-center'><?php echo $solicitante;?></td>
                                                <td class='text-center'><?php echo $autorizo;?></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=5><span class="pull-right">
					<?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>