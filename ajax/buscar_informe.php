<?php


	
	include('is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';	
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code       
        $mes = mysqli_real_escape_string($con,(strip_tags($_REQUEST['mes'], ENT_QUOTES)));
        $tipo = mysqli_real_escape_string($con, (strip_tags($_REQUEST['tipo'], ENT_QUOTES)));
        if(isset($_REQUEST['ano']) &&  $_REQUEST['ano'] !== NULL){
            $ano = mysqli_real_escape_string($con,(strip_tags($_REQUEST['ano'], ENT_QUOTES)));
        }
        else{
            $ano = NULL;
        }
        
		 $aColumns = array('fecha');//Columnas de busqueda
		 $sTable = "historial";
                 $sTable2 = "products";
		 $sWhere = "WHERE historial.fecha >= '$ano-$mes-01' AND historial.fecha <='$ano-$mes-31' AND historial.movimiento = '$tipo' AND  historial.id_producto = products.id_producto";	
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 50; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable,$sTable2 $sWhere");
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './informe.php';
		//main query to fetch the data
		$sql="SELECT historial.fecha, products.codigo_producto, products.nombre_producto, historial.centrocosto, historial.cantidad, (case when products.precio_producto = '#N/D' then 0 else products.precio_producto end) precio_producto FROM  $sTable , $sTable2 $sWhere ORDER BY 1 ASC LIMIT $offset,$per_page";
                
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			
			?>

                        <form action="informe_excel.php" method="post">
                            <input type="hidden" name="mes" value="<?php echo $mes; ?>">
                            <input type="hidden" name="ano" value="<?php echo $ano; ?>">
                            <input type="hidden" name="tipo" value="<?php echo $tipo; ?>">
                            <input type="submit" value="Informe en EXCEL" class="btn btn-primary">
                        </form>
                        
                        <br>
                        <br>
			<div class="table-responsive table-bordered">
			  <table class="table">
				<tr  class="success">
					<th>FECHA</th>
                                        <th>KM</th>
					<th>DESCRIPCIÓN</th>
                                        <th>OBRA</th>
                                        <th>CANTIDAD</th>
                                        <th>P.UNITARIO</th>
                                        <th>TOTAL</th>
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
						$fecha=date('d/m/Y', strtotime($row['fecha']));
						$km=$row['codigo_producto'];
                                                $nombre_producto=$row['nombre_producto'];
                                                $obra=$row['centrocosto'];
                                                $cantidad=$row['cantidad'];
                                                $precio = $row['precio_producto'];
					?>
					<tr>
						
						<td><?php echo $fecha; ?></td>
                                                <td><p class="text-uppercase"><?php echo $km; ?></p></td>
                                                <td><p class="text-uppercase"><?php echo $nombre_producto; ?></p></td>
                                                <td><p class="text-uppercase"><?php echo $obra; ?></p></td>
                                                <td><?php echo $cantidad; ?></td>
                                                <td><?php echo number_format(str_replace(",", ".", $precio), 2); ?></td>
                                                <td><?php echo number_format((str_replace(",", ".", $precio) * $cantidad), 2); ?></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=5><span class="pull-right">
					<?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>

