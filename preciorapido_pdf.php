<?php

        include('ajax/is_logged.php');//Archivo verifica que el usuario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos  
        require_once ("mpdf/mpdf.php");
        
        ?>
<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Formato PDF</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
    </head>
    <body>

        <?php
        
            $q = $_POST['id'];            
        
        $sql = "SELECT * FROM valores WHERE id_producto ='$q' ";
      
        $query = mysqli_query($con, $sql);
        
        if($query){
        $row=mysqli_fetch_array($query);
        if($row>0){
        
       $html = "<h4 class='text-right'>FABRIMETAL</h4>
           <table class='table table-bordered'>              
                <tr class='success'>
                <th colspan='2' class=' text-center'><br><h2>INFORME DE PRECIO</h2><br><br></th>              
                </tr>               
            <tr>
           <td><h3>&nbsp;Código</h3></td> 
           <td><p style='font-size:20px'>&nbsp;".$row['id_producto']."</p></td> 
           </tr>             
            <tr>
           <td><h3>&nbsp;Moneda</h3></td> 
           <td><p style='font-size:20px'>&nbsp;EUROS</td> 
           </tr>
            <tr>
           <td><h3>&nbsp;Precio Venta</h3></td> 
           <td> <p style='font-size:20px'>&nbsp;".number_format($row['precio_venta'],2)." €</p></td> 
           </tr>
           <tr>
           <td><h3>&nbsp;Unidad de medida &nbsp;</h3></td> 
           <td> <p style='font-size:20px'>&nbsp;".$row['unidad']."</p></td> 
           </tr>          
           <tr>
           <td><h3>&nbsp;Fecha</h3></td> 
           <td><p style='font-size:20px'>&nbsp;".date('d-m-Y')."</p></td>
           </tr>
           <tr>
           <td><h3>&nbsp;Hora</h3></td> 
           <td><p style='font-size:20px'>&nbsp;".date("G:i:s")."</p></td>
           </tr>
        </table>        
        <p><strong>NOTA: </strong>EL PRESUPUESTO DEBE SER FIRMADO POR EL JEFE DEL ÁREA.<br>
        ESTE INFORME DEBE SER INCORPORADO AL PRESUPUESTO CORRESPONDIENTE.</p>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p class='text-center'>___________________________________</p>
        <p class='text-center'>FIRMA</p>
         ";
         
         
        }
        }
      
 
        
$mpdf = new mPDF('c');
$css = "<link rel='stylesheet' href='css/bootstrap.min.css'>";
$mpdf->WriteHTML($css,1);
$mpdf->WriteHTML($html,2);
$mpdf->Output();
exit();
        
        ?>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="js/bootstrap.min.js"></script>
        
    </body>
</html>
        
        
        


