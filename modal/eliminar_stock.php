<?php
		if (isset($con))
		{
	?>
<form class="form-horizontal"  method="post">
<!-- Modal -->
<div id="remove-stock" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Salida Stock</h4>
      </div>
      <div class="modal-body">
        
          <div class="form-group">
            <label for="quantity" class="col-sm-2 control-label">Cantidad</label>
            <div class="col-sm-6">
              <input type="number" min="1" name="quantity_remove" class="form-control" id="quantity_remove" value="" placeholder="Cantidad" required="required">
            </div>
          </div>
          <div class="form-group">
            <label for="reference" class="col-sm-2 control-label">Referencia</label>
            <div class="col-sm-6">
              <input type="text" name="reference_remove" class="form-control" id="reference_remove" value="" placeholder="Referencia" required="required">
            </div>
          </div>
          <div class="form-group">
            <label for="centrocosto_remove" class="col-sm-2 control-label">C. costo</label>
            <div class="col-sm-6">                
                <select class='form-control' name='centrocosto_remove' id='centrocosto_remove' required>
                    <option value="">Selecciona un Centro de Costo</option>
                        <?php 
							$query_centrocosto=mysqli_query($con,"select * from centro_costo order by descripcion_centrocosto");
							while($rw=mysqli_fetch_array($query_centrocosto))	{
								?>
							<option value="<?php echo $rw['nombre_centrocosto'];?>"><?php echo $rw['descripcion_centrocosto'];?></option>			
								<?php
							}
			?>
                </select>               
            </div>
          </div>
          <div class="form-group">
            <label for="solicitante_remove" class="col-sm-2 control-label">Solicitante</label>
            <div class="col-sm-6">
              <input type="text" name="solicitante_remove" class="form-control" id="solicitante_remove" value="" placeholder="Quien solicito" required="required">
            </div>
          </div>
          <div class="form-group">
            <label for="autorizo_remove" class="col-sm-2 control-label">Autorizo</label>
            <div class="col-sm-6">
              <input type="text" name="autorizo_remove" class="form-control" id="autorizo_remove" value="" placeholder="Quien autorizo" required="required">
            </div>
          </div>    
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		<button type="submit" class="btn btn-primary">Guardar datos</button>
      </div>
    </div>

  </div>
</div>
</form>

<?php
		}
	?>