<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: login.php");
		exit;
        }

	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	include("funciones.php");
	
	$active_informe="active";
	$active_clientes="";
	$active_usuarios="";	
	$title="Informe | Inventario Fabrimetal";	
	
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include("head.php");?>
  </head>
  <body>
	<?php
	include("navbar.php");	
	?>
      
      <div class="container">
          <div class="panel panel-success">
              <div class="panel-heading">                  
                  <h4><i class=' glyphicon glyphicon-search'></i> Buscar Informe</h4>
              </div>
              <div class="panel-body">
                  
                  
                  <form class="form-horizontal" role="form" id="datos_cotizacion">

                      <div class="form-group row">
                          <label for="mes" class="col-md-2 control-label">Mes / Año / Tipo</label>
                          <div class="col-md-3">                              
                              <select name="mes" id="mes" class="form-control">
                                  <option value="0">MES</option>
                                  <option value="1">ENERO</option>
                                  <option value="2">FEBRERO</option>
                                  <option value="3">MARZO</option>
                                  <option value="4">ABRIL</option>
                                  <option value="5">MAYO</option>
                                  <option value="6">JUNIO</option>
                                  <option value="7">JULIO</option>
                                  <option value="8">AGOSTO</option>
                                  <option value="9">SEPTIEMBRE</option>
                                  <option value="10">OCTUBRE</option>
                                  <option value="11">NOVIEMBRE</option>
                                  <option value="12">DICIEMBRE</option>
                              </select>
                          </div>
                          <div class="col-md-2">
                              <select name="ano" id="ano" class="form-control">
                                  <option value="0">ANO</option>
                                  <option value="2017">2017</option>
                                  <option value="2018">2018</option>
			                      <option value="2019">2019</option>
			                      <option value="2020">2020</option>
			                      <option value="2021">2021</option>
                              </select>
                          </div>
                          <div class="col-md-2">
                              <select name="tipo" id="tipo" class="form-control">
                                  <option value="0">TIPO</option>
                                  <option value="IN">ENTRADA</option>
                                  <option value="OUT">SALIDA</option>
                              </select>
                          </div>
                          <div class="col-md-3">
                              <button type="button" class="btn btn-default" onclick='load(1);'>
                                  <span class="glyphicon glyphicon-search" ></span> Buscar</button>
                              <span id="loader"></span>
                          </div>

                      </div>

                  </form>
                  <div id="resultados"></div> <!-- Carga los datos ajax -->
                  <div class='outer_div'></div><!-- Carga los datos ajax 		-->
                  
              </div>
          </div>

      </div>
      <hr>
      
	<?php
	include("footer.php");
	?>
	<script type="text/javascript" src="js/informe.js"></script>
  </body>
</html>
