<?php


// --- Codigo de consulta

	include('ajax/is_logged.php');//Archivo verifica que el usario que intenta acceder a la URL esta logueado
	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos	
      
         $mes = mysqli_real_escape_string($con,(strip_tags($_REQUEST['mes'], ENT_QUOTES)));
        $tipo = mysqli_real_escape_string($con,(strip_tags($_REQUEST['tipo'], ENT_QUOTES)));
        if(isset($_REQUEST['ano']) &&  $_REQUEST['ano'] !== NULL){
            $ano = mysqli_real_escape_string($con,(strip_tags($_REQUEST['ano'], ENT_QUOTES)));
        }
        else{
            $ano = NULL;
        }
        
		 $aColumns = array('fecha');//Columnas de busqueda
		 $sTable = "historial";
                 $sTable2 = "products";
		 $sWhere = "WHERE historial.fecha >= '$ano-$mes-01' AND historial.fecha <='$ano-$mes-32' AND historial.movimiento = '$tipo' AND  historial.id_producto = products.id_producto";	
		
		//main query to fetch the data
		$sql="SELECT historial.fecha, products.codigo_producto, products.nombre_producto, historial.centrocosto, historial.cantidad, products.precio_producto FROM  $sTable , $sTable2 $sWhere ORDER BY 1 ASC";
                $fecha = array();
                $km = array();
                $nombre_producto = array();
                $obra = array();
                $cantidad = array();
                $precio = array();
                        
                $query = mysqli_query($con, $sql);                
                while ($row=mysqli_fetch_array($query)){
		array_push($fecha,date('d/m/Y', strtotime($row['fecha']))); 		
                array_push($km, $row['codigo_producto']);
                array_push($nombre_producto,$row['nombre_producto']);
                array_push($obra,$row['centrocosto']);
                array_push($cantidad,$row['cantidad']);
                array_push($precio,$row['precio_producto']);
                
                }               
    

require_once 'PHPExcel/Classes/PHPExcel.php';
                
$excel = new PHPExcel();
$excel->getProperties()
      ->setTitle("Excel")
      ->setDescription("Descripción");
$sheet=$excel->getActiveSheet();
$sheet->setTitle("Reporte");

$sheet->setCellValue("A1","FECHA");
$sheet->setCellValue("B1","KM");
$sheet->setCellValue("C1","DESCRIPCION");
$sheet->setCellValueExplicit("D1","OBRA", PHPExcel_Cell_DataType::TYPE_STRING);
$sheet->setCellValue("E1","CANTIDAD");
$sheet->setCellValue("F1","PRECIO_UNITARIO");
$sheet->setCellValue("G1","TOTAL");

$count_query   = mysqli_query($con, "SELECT count(*) AS numrows FROM $sTable,$sTable2 $sWhere");
$row= mysqli_fetch_array($count_query);
$numrows = $row['numrows'];

for($i=2; $i <= $numrows + 1 ; $i++){
    $sheet->setCellValue("A".$i,$fecha[$i-2]);  
    $sheet->setCellValue("B".$i,$km[$i - 2]); 
    $sheet->setCellValue("C".$i,$nombre_producto[$i - 2 ]);
    $sheet->setCellValue("D".$i,$obra[$i - 2]);
    $sheet->setCellValue("E".$i,$cantidad[$i - 2]);
    $sheet->setCellValue("F".$i,$precio[$i - 2]);
    $sheet->setCellValue("G".$i,($precio[$i - 2]*$cantidad[$i - 2]));
}


header("Content-Type: application/vnd.ms-excel");
$nombre = "reporte".date("Y-m-d H:i:s");
header("Content-Disposition: attachment; filename=\"$nombre.xls\"");
header("Cache-Control: max-age=0");
$writer = PHPExcel_IOFactory::createWriter($excel, "Excel5");
$writer->save("php://output");

