	<?php
		if (isset($title))
		{
	?>
<nav class="navbar navbar-default ">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Fabrimetal Stock</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php if (isset($active_productos)){echo $active_productos;}?>"><a href="stock.php"><i class='glyphicon glyphicon-barcode'></i> Inventario</a></li>
        <li class="<?php if (isset($active_categoria)){echo $active_categoria;}?>"><a href="categorias.php"><i class='glyphicon glyphicon-tags'></i> Categorías</a></li>
        <li class="<?php if (isset($active_centrocosto)){echo $active_centrocosto;}?>"><a href="centrocosto.php"><i class='glyphicon glyphicon-usd'></i> C. Costo</a></li>
        <li class="<?php if (isset($active_historial)){echo $active_historial;}?>"><a href="historial.php"><i class='glyphicon glyphicon-list-alt'></i> Historial</a></li>
        <li class="<?php if (isset($active_precios_rapido)){echo $active_precios_rapido;}?>"><a href="precios_rapido.php"><i class='glyphicon glyphicon-euro'></i> Precios</a></li>
        <li class="<?php if (isset($active_informe)){echo $active_informe;}?>"><a href="informe.php"><i class='glyphicon glyphicon-calendar'></i> Informe</a></li>
       </ul>
      <ul class="nav navbar-nav navbar-right">        
		<li><a href="login.php?logout"><i class='glyphicon glyphicon-off'></i> Salir</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
	<?php
		}
	?>