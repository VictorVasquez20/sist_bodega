<?php
	
	session_start();
	if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
        header("location: login.php");
		exit;
        }
	
	/* Connect To Database*/
	require_once ("config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$active_historial="active";
	$title="Historial | Fabrimetal";
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <?php include("head.php");?>
  </head>
  <body>
	<?php
	include("navbar.php");
	?>
	
    <div class="container">
	<div class="panel panel-success">
		<div class="panel-heading">		    
			<h4><i class=' glyphicon glyphicon-search'></i> Buscar Historial</h4>
		</div>
		<div class="panel-body">		
			
			<form class="form-horizontal" role="form" id="datos_cotizacion">
				
						<div class="form-group row">
							
							<div class="col-md-4">
                                                            <label for="q" class="col-md-2 control-label">Historial</label>
                                                            <input type="text" class="form-control text-uppercase" id="q" placeholder="KM del producto">
							</div>
                                                        <div class='col-md-4'>
                                                            <label>Filtrar por movimiento</label>
                                                            <select class='form-control' name='movimiento' id='movimiento' >
                                                                <option value="">Selecciona un movimiento</option>                                                                
                                                                <option value="IN">IN</option>			
                                                                <option value="OUT">OUT</option>  
                                                            </select>
                                                        </div>
							<div class="col-md-4"> 
                                                            <br>                                                             
								<button type="button" class="btn btn-default" onclick='load(1);'>
									<span class="glyphicon glyphicon-search" ></span> Buscar</button>
								<span id="loader"></span>
							</div>
							
						</div>
				
				
				
			</form>                      
				<div id="resultados"></div> <!-- Carga los datos ajax -->
				<div class='outer_div'></div><!-- Carga los datos ajax 		-->
			
  </div>
</div>
		 
	</div>
	<hr>
	<?php
	include("footer.php");
	?>
        <script type="text/javascript" src="js/historial.js"></script>
  </body>
</html>

